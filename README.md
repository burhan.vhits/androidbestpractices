
# Android Best Practices

This README describes the Best Practices should be used for Android App Development.


## Required Tools

- Android Studio Chipmunk (2021.2.1) or Later

### Development Components Overview
| Description|Component Details|
|:------------:|:-----------------:|
|Programming Language|Kotlin 1.7.10 or later|
|Architecture Pattern|MVVM|
|Dependency Injection Framework|Hilt|
|Navigation Component|Navigation Graph|
|Built with Single Activity|And All Sub Fragments|
|Network Library|Retrofit 2.9.0 or Later|
|Version Control System||Gitlab




## Why These ?

### Kotlin

Here are some reasons preferring Kotlin to Java:

- Kotlin is concise, saving time that you'd otherwise spend writing boilerplate code in Java.
- Kotlin handles nulls. A null in Java can crash a program if you haven't prepared for it. In Kotlin, you can add a simple operator to variables that may be null to prevent these crashes.
- It's easy to switch to Kotlin. Kotlin is fully compatible with Java, so you don't have to change all your code at once. You can fully migrate an application to Kotlin.
- Kotlin has coroutines that make asynchronous programming simple and efficient.

### MVVM

MVVM separates your view (i.e. Activitys and Fragments) from your business logic. MVVM is enough for small projects, but when your codebase becomes huge, your ViewModels start bloating. Separating responsibilities becomes hard.

MVVM with Clean Architecture is pretty good in such cases. It goes one step further in separating the responsibilities of your code base. It clearly abstracts the logic of the actions that can be performed in your app.

### Hilt

- Reduced boilerplate
- Decoupled build dependencies
- Simplified configuration
- Improved testing
- Standardized components

### Navigation Graph

- Automatic handling of fragment transactions
- Correctly handling up to and back by default.
- Default behaviours for animations and transitions
- Deep linking as a first-class operation.
- Type safety when passing information while navigating.


## Programming Rules

### Naming Convention

We are following the below-naming conventions to achieve Readability, Clarity, Consistency in code.

**Application Namespace:** 

Use a prefix that identifies the application (example: `TP` for an app called `Test Project`). Use this prefix when naming classes, Activities, Fragments wherever needed.

**Package Names**

- Package name should be in `snake_case` and self descriptive.

**Class Names**

- Class Name should be in `PascalCase` and self descriptive.

|Do's & Don'ts|❌ ✅|
|:--|:--:|
| “Person_Model”,”_PersonModel” |❌|
| ”PersonModel_”,”Person Model”|❌|
| “personModel”, ”PERSONMODEL”|❌|
| ”personmodel”|❌|
|“PersonModel”, “LoginFragment”|✅|
|”UserListAdapter”,"PermissionListener"|✅|

**Variable Names**

- Variable name should be in `lowerCamelCase`.
- Access Specifiers should be maintained properly.
- Private Members should be define with prefix `_` 
- `lateinit`, `var`, `val`,`?:`,`!!` should me maintained properly.
- Constants should be define in `UPPER_SNAKE_CASE`.

|Do's & Don'ts|❌ ✅|
|:--|:--:|
| “Person_Model”, ”_PersonModel” |❌|
| ”PersonModel_”, ”Person_model”|❌|
| “personModel”, “loginVM”, “loginBinding”|✅|

**Method Names**
- Method name should be in `lowerCamelCase`.
- Access Specifiers should be maintained properly
- Method name should have appropraiate name, method returning value should define its own functionality.

|Do's & Don'ts|❌ ✅|
|:--|:--:|
|“get_data_person_List()”, ”invokeIsLogin_Flag()” |❌|
|”getPersonList()”, ”invokeLogin()”|✅|
|“isCheckedIn()”, “getUserById()”|✅|
|“getRemoteDate()”, storeList(list:List<PersonModel>)|✅|

**Enums**

- Use Enums wherever possible 
- Enum name should be in `PASCAL_CASE`
- Overload Constructors of Enum as needed
```
enum class UserRole(private var roleName: String) {
    HR("HR"), RECEPTION("Receptionist");
    fun toName() = roleName
}

....
UserRole.RECEPTION.toName() // will return "Receptionist"
....
```

### Code Documentation
- Each class, methods, utility functions must have documentation about it's work with comments.
- Use of SingleLine for variables or any flags.
- Use Multiline comments about logic explanation like above loops and conditional statements as necessary
- Use Document comments above functions and classes.

**Single Line Comment**
```
//      Casting Parent Activity as BaseActivity.
        (activity as BaseActivity).userLogout()
```

**Multi Line Comment**
```
    /* If Response Status Will be 1 then Execute Success Statement, Else Error wil be appear */
    if (resp.body()!!.status == 1) {
        emit(Resource.success(resp.body()!!.data, resp.body()!!.error_msg!!))
    } else {
        emit(Resource.error(resp.body()!!.error_msg!!))
    }
```

**Document Comment**
```
    /**
     * Will trigger when user clicks on Logout Button.
     * */
    override fun onLogoutClicked() {
        (activity as BaseActivity).userLogout()
    }
```

### Directory Structure

- We have predefined directory structure needs to be follow for easy understanding and better maintaining of code.
```
com/
└── app/
    └── project_name/
        ├── base/
        │   └── # Contains BaseActivity, BaseFragment 
        ├── di/
        │   ├── Application.kt # Contains Application Class
        │   └── AppModule.kt # Contains Application Level Instance Provider created using DI framowrk - Hilt
        ├── data/
        │   ├── dao/
        │   │   └── # Contains DAO Logic while using RoomDB
        │   ├── models/
        │   │   └── # Contains Data Classes and Models
        │   └── enums  /
        │       └── # Contains Enums
        ├── network/
        │   └── # Contains Network Config, Base Response
        ├── repositories /
        │   └── # Contains all rest apis execution, and will be created by modules
        ├── ui/
        │   └── screen_name/
        │       └── # Contains Screen Related Fragments, ViewModels, Navigators, Adapters
        └── utils /
            ├── custom_views/
            │   └── # Contains custom views as needed
            ├── helpers/
            │   └── # Contains helper classes e.g. PrefrenceHelpers, PermissionHelpers
            ├── Extensions.kt # Contains custom extensions
            ├── Constants.kt # Contains all the constants
            ├── BindingAdapter.kt # Contains Global Binding Adapters
            └── TypeConverters.kt # Contains All Type Converters as required while using RoomDB      
```

